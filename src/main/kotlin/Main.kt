// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import org.apache.poi.ss.usermodel.CellValue
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.FileInputStream
import java.io.FileOutputStream
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import kotlin.math.round
import kotlin.math.roundToInt

fun formatDateForSheet(date: LocalDateTime): String {
    val formatter = DateTimeFormatter.ofPattern("dd-MM")
    return date.format(formatter)
}

fun main() {

    val filename = "bootleg_ze"

    val path = "C:\\Users\\fisen\\OneDrive\\Documents\\${filename}.xlsx"

    val timestamp = LocalDate.now().format(DateTimeFormatter.ISO_DATE)

    val newPath = "C:\\Users\\fisen\\OneDrive\\Documents\\${filename}_${timestamp}.xlsx"

    val inputStream = FileInputStream(path)

    val xlWb = WorkbookFactory.create(inputStream)

    val dataformat = xlWb.createDataFormat()
    val style = xlWb.createCellStyle()
    style.dataFormat = dataformat.getFormat("hh:mm")

    inputStream.close()

    val tableData = readExcel(xlWb)
    val startDate = LocalDate.of(2023, 1, 1).atStartOfDay()
    val endDate = LocalDate.of(2023, 1, 25).atStartOfDay()

    val contracts = ArrayList<String>()

    print(tableData)

    for (row in tableData) {
        if (row.date < startDate || row.date >= endDate) {
            continue
        }
        for (entry in row.workEntries) {
            if (entry.contract !in contracts)
                contracts.add(entry.contract)
        }
    }

    println(contracts)

    val sheetName = "${formatDateForSheet(startDate)} - ${formatDateForSheet(endDate)}"
    var sheet = xlWb.getSheet(sheetName)
    if (sheet != null) {
        val sheetIndex = xlWb.getSheetIndex(sheet)
        xlWb.removeSheetAt(sheetIndex)

    }

    sheet = xlWb.createSheet(sheetName)


    var currentRow = 3

    var totalRowCount = ChronoUnit.DAYS.between(startDate, endDate).toInt()
    val headerRow = sheet.createRow(2)

    var currentDate = LocalDateTime.from(startDate)
    while (currentDate <= endDate) {
        val row = sheet.createRow(currentRow)
        val dateCell = row.createCell(0)
        dateCell.setCellValue(currentDate.format(DateTimeFormatter.ISO_DATE))

        val timesheetRow = tableData.firstOrNull { it.date == currentDate }

        currentRow++
        currentDate = currentDate.plusDays(1)

        if (timesheetRow == null) {
            continue
        }

        var previousEndTime = timesheetRow.startTime

        for ((index, workEntry) in timesheetRow.workEntries.withIndex()) {
            val contractIndex = contracts.indexOf(workEntry.contract)
            val offset = contractIndex * 8

            val headerCell = headerRow.createCell(offset + 1)
            headerCell.setCellValue(workEntry.contract)
            val startTimeCell = row.createCell(offset + 1)
            startTimeCell.cellStyle = style
            if (index == 0) {
                val startTimeForExcel = (Duration.between(LocalTime.of(0, 0), timesheetRow.startTime).toSeconds() / 3600.0) / 24.0
                startTimeCell.setCellValue(startTimeForExcel)
            } else {
                val startTimeForExcel = (Duration.between(LocalTime.of(0, 0), previousEndTime).toSeconds() / 3600.0) / 24.0
                startTimeCell.setCellValue(startTimeForExcel)
            }

            val endTimeCell = row.createCell(offset + 2)
            endTimeCell.cellStyle = style
            if (index == timesheetRow.workEntries.size - 1) {
                val endTimeForExcel = (Duration.between(LocalTime.of(0, 0), timesheetRow.endTime).toSeconds() / 3600.0) / 24.0
                endTimeCell.setCellValue(endTimeForExcel)
            } else {
                var hoursOnContract = (((timesheetRow.totalWorkTime / 100) * workEntry.timeSplit) * 4).roundToInt() / 4.0
                if (index == 0) {
                    hoursOnContract += timesheetRow.breakTime
                }
                val duration = Duration.ofNanos(((hoursOnContract) * 60).toLong() * (Duration.ofMinutes(1).toNanos()))
                val endTime = previousEndTime + duration
                val endTimeForExcel = (Duration.between(LocalTime.of(0, 0), endTime).toSeconds() / 3600.0) / 24.0
                endTimeCell.setCellValue(endTimeForExcel)
                previousEndTime = endTime
            }

            val breakTimeCell = row.createCell(3 + offset)
            if (index == 0) {
                breakTimeCell.setCellValue(timesheetRow.breakTime)
            } else {
                breakTimeCell.setCellValue(0.0)
            }

            val topicCell = row.createCell(4 + offset)
            topicCell.setCellValue(workEntry.topics.joinToString(","))

            val workplaceCell = row.createCell(7 + offset)
            workplaceCell.setCellValue(timesheetRow.workplace)

        }

    }

    val outputStream = FileOutputStream(newPath)

    xlWb.write(outputStream)

    outputStream.close()


}
