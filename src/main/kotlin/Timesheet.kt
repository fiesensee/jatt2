import java.time.LocalDateTime
import java.time.LocalTime

data class Timesheet(val rows: List<TimesheetRow>)

data class TimesheetRow(
    val date: LocalDateTime,
    val startTime: LocalTime,
    val endTime: LocalTime,
    val breakTime: Double,
    val workEntries: List<WorkEntry>,
    val workplace: String,
    val totalWorkTime: Double
)

data class WorkEntry(val contract: String, val timeSplit: Int, val topics: List<String>)