import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.FileInputStream
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

val dataFormatter = DataFormatter()

val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("M/d/yy")

val timeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("H:mm")

fun readExcel(xlWb: Workbook): List<TimesheetRow> {
    val rows = ArrayList<TimesheetRow>()

    val evaluator = xlWb.creationHelper.createFormulaEvaluator()

    val xlWs = xlWb.getSheetAt(0)

    for (row in xlWs) {
        val values = ArrayList<String>()
        for (cell in row) {
            values.add(dataFormatter.formatCellValue(cell, evaluator).trim())
        }

        if (values[0] == "Datum" || values.size != 9) continue

        val contracts = values[4].split(";")
        val timeSplits = values[5].split(";")
        val topics = values[6].split(";")

        val workEntries = ArrayList<WorkEntry>()

        println(values[0])
        for (i in contracts.indices) {
            val workEntry = WorkEntry(
                contract = contracts[i],
                timeSplit = timeSplits[i].toInt(),
                topics = topics[i].split(",")
            )
            workEntries.add(workEntry)
        }


        val timesheetRow = TimesheetRow(
            date = LocalDate.parse(values[0], dateFormatter).atStartOfDay(),
            startTime = LocalTime.parse(values[1], timeFormatter),
            endTime = LocalTime.parse(values[2], timeFormatter),
            breakTime = values[3].toDouble(),
            workEntries = workEntries,
            workplace = values[7],
            totalWorkTime = values[8].toDouble()
        )
        rows.add(timesheetRow)
    }

    return rows
}